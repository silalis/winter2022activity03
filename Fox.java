public class Fox
{
	public String type;
	public int strength;
	public String origin;
	
	public void questions(String type)
	{
		if (type.equals("Arctic"))
		{
			System.out.println("The Arctic fox says: Why would the winds blow thus even though it is so cold already?"); 
		}		
		else
		{
			System.out.println("The Mystic fox says: Tricking people is fun is it not? I have simply decided to live a little longer for that basic pleasure.");
			
		} 
	}
	
	public void greeting(String type)
	{
		if (type.equals("Arctic"))
		{
			System.out.println("The Arctic fox says: Hello...you would do well to keep away.");
		}
		else
		{
			System.out.println("The Mystic fox says: Ah hello, would you like to eat this meal. No, it is not composed of real ingredients, why do you ask?");
		}
	}
}