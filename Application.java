import java.util.Scanner;
public class Application
{
	public static void main (String[] args) 
	{
		Scanner reader = new Scanner(System.in);
		
		Fox arctic = new Fox();
		arctic.type="Arctic";
		arctic.strength=5;
		arctic.origin="North Pole";
		
		Fox mystic = new Fox();
		mystic.type="Mystic";
		mystic.strength=7;
		mystic.origin="East Asia";
		
		Fox[] leash = new Fox[3];
		leash[0] = arctic;
		leash[1] = mystic;
		leash[2] = new Fox();
		leash[2].type="Red";
		leash[2].strength=2;
		leash[2].origin="Mild forests";
		System.out.println(leash[2].type);
		System.out.println(leash[2].strength);
		System.out.println(leash[2].origin);
		
		Fox foxes = new Fox();
		
		System.out.println("Which fox would you like to greet? (Arctic or Mystic)");
		String greet = reader.next();
		
		if (greet.equals("Arctic"))
		{
			foxes.greeting(arctic.type);
		}
		else 
		{
			foxes.greeting(mystic.type);
		}
		
		System.out.println("Which fox would you like to ask about their origin?");
		String question = reader.next();
		
		if (question.equals("Arctic"))
		{
			foxes.questions(arctic.type);
		}
		else
		{
			foxes.questions(mystic.type);
		}
	}
}